import shlex, subprocess, re, os, time, http.client
from datetime import datetime
from platform import system

"""
def timed_execution(function):
    def wrapper(*args, **kwargs):
        t1 = time.time()
        function(*args, **kwargs)
        t2 = time.time()
        print('time taken : %f seconds' % (t2 - t1) + "\n")
    return wrapper

import threading
def threaded_call(function):
    def wrapper(*args, **kwargs):
        thr = threading.Thread(target=function, args=args, kwargs=kwargs)
        thr.start()
    return wrapper
"""

try:
    import simplejson as json
except ImportError:
    import json

""" Config data dictionary with dafault values
"""
config_default = {
    'system': {
        'username': 'www-data',
        'auth': {
            "test": "test"
        }
    },
    'platforms': {
        'local': {
            'type': 'gpio'
        },
        'tasmota1': {
            'type': 'tasmota',
            'host': '192.168.14.1'
        }
    },
    'outlets': {
        'outlet1': {
            'active': True,
            'name': 'OUTLET1_DESC',
            'timers': [
                {
                    'action': 'on',
                    'time': '8:00'
                },
                {
                    'action': 'off',
                    'time': '8:01'
                }
            ],
            'pin': 12
        },
        'outlet2': {
            'active': True,
            'name': 'Every working days event',
            'timers': [
                {
                    'action': 'on',
                    'time': '12:00',
                    'weekday': [1,2,3,4,5]
                },
                {
                    'action': 'off',
                    'time': '12:01',
                    'weekday': [1,2,3,4,5]
                }
            ],
            'pin': 16
        },
        'outlet3': {
            'active': False
        },
        'outlet4': {
            'active': False
        },
    },
    'thermometers': {
        'therm1': {
            'romid': '28:00:00:00:00:00:00:00',
            'imperial': False,
            'correction': 0.8
        }
    },
}


class Relay:
    def __init__(self):
        """ Use main program path as base directory
            path_home is programm whereabouts
            path_config is path to relay.json
            path_init is path to relays start position utility
            path_images is dummy for themes ('default' is only theme now)
        """
        self.path_home = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
        self.path_config = os.path.join(self.path_home, 'db/relay.json')
        self.path_init   = os.path.join(self.path_home, 'bin/relay_init.py')
        self.path_switch = os.path.join(self.path_home, 'bin/relay_set.py')
        self.path_images = os.path.join(self.path_home, 'html/images')

        """ Read config file
        """
        self.config = self.config_json(self.path_config, config_default)
        self.platforms = self.get_platforms()
        self.outlets = self.get_outlets()
        self.gpio_status = { 'on' : 0, 'off' : 1 }

        self.known_states = ['on', 'off', 'toggle']
        self.modes_number = 3

        """ Prepare OS depended values
        """
        osname = system()
        if osname == 'Linux':
            """ RPi/OPi.GPIO utility
            self.cmd_gpio_mode = '/usr/local/bin/gpio -1 mode {} {}'
            self.cmd_gpio_set = '/usr/local/bin/gpio -1 write {} {}'
            self.cmd_gpio_get = '/usr/local/bin/gpio -1 read {}'
            self.cmd_gpio_toggle = None
            """
            """ gpiod interface utility. Run: sudo apt install gpiod """
            self.cmd_gpio_mode = None
            self.cmd_gpio_set = '/usr/bin/gpioset 0 {} {}'
            self.cmd_gpio_get = '/usr/bin/gpioget 0 {}'
            self.cmd_gpio_toggle = None
        elif osname == 'FreeBSD':
            """ Built-in gpioctl utility """
            self.cmd_gpio_mode = '/usr/sbin/gpioctl -pc {} {}'
            self.cmd_gpio_set = '/usr/sbin/gpioctl -p {} {}'
            self.cmd_gpio_get = '/usr/sbin/gpioctl -p {}'
            self.cmd_gpio_toggle = '/usr/sbin/gpioctl -pt {}'
        else:
            exit(f"I don't know how to use gpio on {osname}")


    """ Get level of the gpio pin
    """
    def get_gpio_level(self, number):
        result = subprocess.run(shlex.split(self.cmd_gpio_get.format(number)), capture_output=True)
        """ Try to get pin state """
        output = result.stdout.decode()
        try:
            return int(output)
        except ValueError:
            print(output)
            exit(12)


    """ Get gpio switch status, "on" or "off"
    """
    def get_gpio(self, number):
        pin_level = self.get_gpio_level(number)

        return self.known_states[pin_level]


    """ Switch the gpio connected relay. Result: 'on' or 'off'
    """
    def set_gpio(self, number, state):
        if self.cmd_gpio_mode is not None:
            result = subprocess.run(shlex.split(self.cmd_gpio_mode.format(number, 'out')))

        st = state.lower();
        if st == 'toggle':
            if self.cmd_gpio_toggle is not None:
                result = subprocess.run(shlex.split(self.cmd_gpio_toggle.format(number)))
                return self.get_gpio(number)
            else:
                """ Try to get pin state """
                pin_level = self.get_gpio_level(number)
                """ Invert pin state for toggle action """
                pin_level = abs(pin_level - 1)
        else:
            pin_level = self.gpio_status[st]

        result = subprocess.run(shlex.split(self.cmd_gpio_set.format(number, pin_level)))

        """ Read the result from the pin, just in case """
        return self.get_gpio(number)


    """ Send a POWER request to the tasmota host and return an answer
        as a string
    """
    def cmd_tasmota(self, host, cmd):
        headers = {'Host': host, 'Content-type': 'application/json'}
        conn = http.client.HTTPConnection(host, 80, timeout=1)
        conn.request('GET', f'/cm?cmnd={cmd}', headers=headers)
        response = conn.getresponse()
        data = response.read()
        conn.close()

        return data


    """ Switch the remote tasmota relay
    """
    #@timed_execution
    def set_tasmota(self, host, number, state):
        data = self.cmd_tasmota(host, f'POWER{number}%20{state}')
        status = json.loads(data)

        return status[f'POWER{number}'].lower()


    """ Get tasmota switch status, "on" or "off"
    """
    def get_tasmota(self, host, number):
        data = self.cmd_tasmota(host, f'POWER{number}')
        status = json.loads(data)

        return status[f'POWER{number}'].lower()


    """ Get a status of the outlet by its id, "on" or "off"
    """
    def get_status(self, outlet_id):
        outlet = self.get_outlet(outlet_id)
        platfm = self.get_outlet_platform(outlet)
        number = self.get_outlet_number(outlet)
        if platfm["type"] == "gpio":
            status = self.get_gpio(number)
        elif platfm["type"] == "tasmota":
            host = platfm["host"]
            status = self.get_tasmota(host, number)

        return status


    def action_outlet(self, state, action, *args):
        mode_match = re.match(rf'^mode([1-{self.modes_number}])$', state, re.ASCII | re.IGNORECASE)
        if mode_match:
            mode = int(mode_match.group(1))
            if mode > 0:
                repeat = mode * 2 - 1
                """ If just turned off sleep ~10 sec (9.5 + 0.5) to reset mode sequence """
                if action(*args, 'toggle') == 'off':
                    time.sleep(10)
                else:
                    repeat -= 1
    
                while repeat > 0:
                    """ half the second enough for delay """
                    time.sleep(.5)
                    action(*args, 'toggle')
                    repeat -= 1
        elif state.lower() in self.known_states:
            action(*args, state)
        else:
            print(f'Don\'t know what to do for "{state}". Ignored');


    """ Get relay type and switch the relay accordingly
    """
    def switch_outlet(self, outlet_id, state, force=False):
        if state is None:
            print(f'Source "{outlet_id}" has an empty list of timers. Ignored')
            return

        outlet = self.get_outlet(outlet_id)
        platfm = self.get_outlet_platform(outlet)
        number = self.get_outlet_number(outlet)

        if force or self.outlet_is_active(outlet):
            if platfm['type'] == 'gpio':
                self.action_outlet(state, self.set_gpio, number)
            elif platfm['type'] == 'tasmota':
                host = platfm['host']
                self.action_outlet(state, self.set_tasmota, host, number)
        else:
            print(f'Source "{outlet_id}" is not active. Ignored')


    """ Get platforms collection as dictionary
    """
    def get_outlets(self):
        """ Check if any outlet is defined """
        try:
            outlets = self.config["outlets"]
        except KeyError as e:
            print("relay.db: No outlets defined")
            exit(4)

        for name, outlet in outlets.items():
            """ Check the pin numbers """
            if "pin" not in outlet:
                print("relay.db: \"{}\" for outlet \"{}\" is not defined".format("pin", name))
                exit(5)

            """ Default platform is "local" """
            if "platform" not in outlet:
                outlet["platform"] = "local"

            """ Default active value is true """
            if "active" not in outlet:
                outlet["active"] = True

            """ Create an empty timers list if not any """
            if "timers" not in outlet:
                outlet["timers"] = {}

        return outlets


    """ Get platforms collection as dictionary
    """
    def get_platforms(self):
        """ Check if any platform is defined """
        try:
            platforms = self.config["platforms"]
        except KeyError as e:
            print("relay.db: No relays platform defined")
            exit(6)

        for name, platform in platforms.items():
            try:
                _type = platform["type"]
            except KeyError as e:
                _type = "gpio"
                platform["type"] = _type

            if _type == "tasmota":
                if "host" not in platform:
                    platform["host"] = "192.168.14.1"
            elif _type == "gpio":
                pass
            else:
                print("relay.db: Type \"{}\" for platform \"{}\" is not supported".format(_type, name))
                exit(7)

        return platforms


    """ Get the outlet definition as dictionary
    """
    def get_outlet(self, outlet_id):
        """ Check if the outlet is defined """
        try:
            outlet = self.outlets[outlet_id]
        except KeyError as e:
            print(f'relay.db: No outlet "{outlet_id}" is defined')
            exit(8)

        return outlet


    """ Is outlet active
    """
    def outlet_is_active(self, outlet):
        active = outlet["active"]

        return active


    """ Get outlet pin number
    """
    def get_outlet_number(self, outlet):
        number = outlet["pin"]

        return number


    """ Get outlet platform name
    """
    def get_outlet_platform(self, outlet):
        """ Default platform for outlets is "local" """
        platfm = outlet.get("platform", "local")

        """ Check if the platform is defined """
        try:
            platform = self.platforms[platfm]
        except KeyError as e:
            print(f'relay.db: No platform "{platfm}" defined')
            exit(9)

        return platform


    """ Get outlet timers
    """
    def get_outlet_timers(self, outlet):
        timers = outlet["timers"]

        return timers


    """ Get datetime time for the timer
    """
    def get_timer_time(self, timer):
        try:
            time = datetime.strptime(timer["time"], "%H:%M").time()
        except KeyError as e:
            print("relay.db: No \"{}\" in {} is defined".format("time", timer))
            exit(10)

        return time


    """ Get action string for the timer (typically, "on" or "off")
    """
    def get_timer_action(self, timer):
        try:
            action = timer["action"].lower()
        except KeyError as e:
            print("relay.db: No \"{}\" in {} is defined".format("action", timer))
            exit(11)

        return action


    """ Reads the config.json file as config or creates it if not exists one
        config_json(config_file[, default_config_dictionary])
    """
    def config_json(self, path_config, config_data = None):

        if os.path.isfile(path_config) and os.access(path_config, os.R_OK):

            try:
                with open(path_config, 'r') as config_json_file:
                    config_data = json.load(config_json_file)

            except IOError as e:
                print(f'Unable to read a configuration from file {path_config}')
                exit(1)

        elif config_data is not None:

            try:
                with open(path_config, 'w') as config_json_file:
                    json.dump(config_data, config_json_file, indent=4, ensure_ascii=False)

                print(f'Configuration file "{path_config}" created')

            except IOError as e:

                print(f'Unable to write a configuration to the file "{path_config}"')
                exit(2)

        else:
                print(f'Unable to read a configuration from file "{path_config}" neither default one is defined')
                exit(3)

        config = config_data

        return config

