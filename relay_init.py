#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import shlex, subprocess
from datetime import datetime, timedelta
from time import sleep
import relay
from relay import Relay

class relay_init(Relay):

    def __init__(self):
        super().__init__()

        current_datetime = datetime.now()
        current_time = current_datetime.time()
        self.current_date = current_datetime.date()

        for outlet_id, outlet in self.outlets.items():
            """ Play for each outlets """

            action = 'off'

            """ Active ones only
            """
            if self.outlet_is_active(outlet):
                """ Create a week timers list for each outlet ordered by time descending
                """
                timers_rev = self.outlet_week_actions(outlet)
                for timer in timers_rev:
                    """ Play the action dictionary (descending) until
                        'current_time' is greater than timer cursor (already a datetime object).
                    """
                    if current_datetime > timer["time"]:
                        action = self.get_timer_action(timer)
                        break
                else:
                    """ If 'current_time' lesser any timers one for same outlet
                        then take a last resort timer (last one for today as
                        for a week long period).
                        The last today timer index of timer_rev is 0 if it exists of course.
                    """
                    action = self.get_timer_action(timers_rev[0]) if len(timers_rev) > 0 else None

                sleep(1)
                self.switch_outlet(outlet_id, action)


    """ Create a list of actions for the "outlet" for a week """
    def outlet_week_actions(self, outlet):
        week = []
        timers = self.get_outlet_timers(outlet)
        for timer in timers:
            """ Collect the week timer list for last week (with today)
                and change the timers 'time' parameters by counted
                'datetime' objects for best sorting and compare
            """
            for i in range(7):
                i_date = self.current_date - timedelta(days=i)
                i_dow = i_date.isoweekday()
                i_time = datetime.combine(
                        i_date,
                        self.get_timer_time(timer)
                )

                """ If 'weekday' list exists collect only days match it.
                    If no 'weekday' list collect the timer for last 7
                    days
                """
                action = self.get_timer_action(timer)
                try:
                    if i_dow in timer['weekday']:
                        week.append({ 'time': i_time,
                                      'action': action })
                except KeyError:
                    week.append({ 'time': i_time,
                                  'action': action })

        """ Sort the week timers list descending
        """
        timers_rev = sorted(week, key=lambda w: w['time'], reverse=True)
        return timers_rev


if __name__ == "__main__":
    sleep(30)
    relay_init()
