#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from crontab import CronTab
import relay
from relay import Relay

delay = {};

class relay_cron(Relay):

    def __init__(self):
        super().__init__()

        try:
            crontab = CronTab(user=self.config['system']['username'])
        except IOError as err:
            exit('{}\nTry to edit a "system"->"username" parameter of a config file {}'.format(str(err), self.path_config))

        crontab.remove_all()

        """ Init the relays on system boot
        """
        job = crontab.new(
            command="{} && {}".format('sleep 60', self.path_init),
            comment='Run on each reboot')
        job.every_reboot()

        """ Ping every minute for wi-fi keepalive.
            Not needed for wired ethernet
        """
        """
        ping = crontab.new(
            command='/bin/ping -c 3 -i 1 -q 192.168.5.1',
            comment='Ping to prevent WiFi to sleep')
        ping.minute.every(1)
        """

        for outlet_id, outlet in sorted(self.outlets.items()):
            """ Play for each outlets """

            if self.outlet_is_active(outlet):
                """ Active one only """
                job = crontab.new()

                timers = self.get_outlet_timers(outlet)
                for timer in timers:
                    time = self.get_timer_time(timer)
                    """ Create a cron job by timer and set time
                    """
                    try:
                        delay[time] += 1
                    except KeyError:
                        delay[time] = 1

                    action = self.get_timer_action(timer)
                    job = crontab.new(
                        command="{} {}={}".format(self.path_switch, outlet_id, action),
                    )

                    job.hour.on(time.hour)
                    job.minute.on(time.minute)

                    """ Set a weekday if it exists for current timer.
                        Do it after job.setall()
                    """
                    if 'weekday' in timer:
                        job.dow.on(*timer['weekday'])

        crontab.write()


relay_cron()


exit(0)
