# relay-pi

----

## About

A couple of utilities for controlling N-channel relay, local and remote.

I wrote them, first of all, for my aquarium, which consists of a `4-channel
relay` with a `block of sockets` and a `SOC-computer` **OrangePi Zero**, which I
used as a GPIO-platform (They placed into a combined case I printed for them).
I use the `4-channel relay` for remote control of lighting and air supply for my
aquariums, which in turn is controlled by the **cron** utility running on the
`SOC-computer`.

There is another project of mine,
[relay-pi-webui](https://gitlab.com/alexandermishin13/relay-pi-webui),
which is a simple framework that implements a graphical interface for relay
control based on the **nginx** and **php-fpm** packages.
Of course, you can use it for something that exactly you need to control, as
well as create your own visualization module for the `relay-pi-webgui`.

Initially, I ran it on the OS **Armbian Linux**, and it worked like a charm.
Now it runs on the OS **FreeBSD**, and it works as well as before.
This project can work successfully on both of them.

## Requirements

- An ARM computer with **gpio** running on Raspbian, Armbian, FreeBSD, etc.;
- An GPIO-wired N-channel relay module or a remote device with **Tasmota**
firmware installed;
- A sufficient number of wires with two female Dupont connectors for the GPIO-
wiring;
- A Network with Internet access for packages installation.

## Description

- **relay.py** - provides a class definition for utilites;
- **relay_init.py** - runs after computer boot ended (and a bit while for
  computer time to be fully synchronized). It parses the **config.json** and
  actualizes states of all relays accordingly;
- **relay_cron.py** - parses the **config.json** and schedules some jobs for
  the **cron** utility. It schedules also a start of **relay_init.py** on boot;
- **relay_scale.py** - parses **config.json** and creates scales with cron jobs
  graphs for every days of week and only be needed if You plan to use
  --relay-pi-webui-- (Obsolete after "relay-pi-webui" learned to do it on its
own using Javascript);
- **relay_set.py** - Switches relays to the required state by their names.

Supported actions for relay are "on", "off", "toggle", "mode1" (same as "on"),
"mode2", "mode3". The “mode#” actions sequentially switch the relay until the
required mode number is selected.

## Installation

- Create a directory for your project (*/srv/\<project\>/*, for example);
- Change the current directory to it;
- Create two folders, *./bin/* for executables and *./db/* for the config file;
- Copy files of the project to the *./bin/* directory then change the current
directory into it;
- Run ```sudo chmod +x relay_*.py``` to make utilities executable;
- Run either *./relay_cron.py* or *./relay_init.py* for the first time and
it will create an example of config file *../db/relay.json*. Obviously you want
to edit it first according to your own needs.

You need to edit *./relay.py* if You like to put *relay.json* to another place.

You need to install the *python* if you still don't have not yet it, and the
*py-python-crontab* module (and the *py-wand* one, if you need the
*relay_scales.py* utility to run).

To do that for the **python** version 3 on **FreeBSD** (the current version is
Python 3.9) try something like:
```shell
sudo pkg install python3
sudo pkg install py39-python-crontab
```
If You have installed **py-simplejson** the package will be tried to load first.
Otherwise, Python's own JSON implementation code will be used.

To install the **py-simplejson** module try:
```shell
sudo pkg install py39-simplejson
```
Add the following two lines to */etc/devfs.conf* to allow **nginx** to manage
GPIO pins:
```
own gpioc0 :www
perm gpioc0 0660
```
...and restart the service:
```shell
sudo service devfs restart
```
If Your platform does not have an RTC, even if you using an `ntpd` service, you
may want to start the `ntpdate` service when your computer boots up.
Just create a file */etc/rc.conf.d/ntpdate* with the followin line inside:
```ini
ntpdate_enable="YES"
```
This will allow the device to set its clock as quickly as possible before it
can run `relay-init.py` script.

FreeBSD is smart enough for run `ntpdate` before `ntpd` at startup when both
time services are activated but on Linux the `ntpdate` service is mostly
obsolete.

## Configure

You can configure this project to work with your devices by editing the file
*db/relay.json*. Let's look at it closer.

The config file have json format. Its most essential parts are represented by
following keys: `system`, `platforms` and `outlets`.

### system


``` json
"system": {
    "username": "www",
    "auth": {
        "user1": "password1",
        "user2": "$5$salt$hash"
    }
},

```

The `system` key contains of two other keys:

- `username`: The name of the user, under which cron jobs will be created and
with whose permissions they will be executed. This is the same username whom
we added to the file */etc/devfs.conf*;
- `auth`: Contains the pairs of names and passwords for `relay-pi-webui` web
authentication.

### platforms

``` json
"platforms": {
    "local": {
        "type": "gpio"
    },
    "tasmota1": {
        "type": "tasmota",
        "host": "192.168.14.1"
    }
},
```

The `platforms` key contains a dictionary of named types of access to your
various devices. You need at least one platform defined for one device.
You are free to choose names for them, but if you have an outlet without a
specified platform, the name "Local" will be used.

For each platform you need to set the "type" parameter and if you omit it,
the "gpio" type will be used. By now only two types supported for platforms:
"gpio" and "tasmota".
If you specified the "type" parameter as "tasmota" you need to add the "host"
parameter, the value of which should be the name of the host or its ip-address.
If you omit parameter "host", ip-address "192.168.14.1" will be used.

### outlets

``` json
"outlets": {
    "outlet1": {
        "active": true,
        "platform": "local",
        "pin": 27,
        "timers": [
            {
                "time": "09:00",
                "action": "on",
                "weekday": [1,2,3,4,5]
            },
            {
                "time": "10:00",
                "action": "off",
                "weekday": [1,2,3,4,5]
            }
        ]
    },
    "outlet2": {
        "active": true,
        "platform": "tasmota1",
        "pin": 1,
        "timers": [
            .......
        ]
    }
},
```

There will be some text.
