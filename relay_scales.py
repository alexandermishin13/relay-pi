#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from datetime import datetime
from wand.image import Image
from wand.color import Color
from wand.drawing import Drawing
import relay
from relay import Relay

class relay_scales(Relay):

    def __init__(self):
        super().__init__()

        color = {
            'workday': 'lightskyblue',
            'holiday': 'lightsalmon'
        }

        """ Play for each outlets
        """
        for outlet_id, outlet in sorted(self.outlets.items()):

            if self.outlet_is_active(outlet):
                """ Active outlets only """
                with Drawing() as draw:
                    timers = self.get_outlet_timers(outlet)
                    scales = {}
                    for day in range(1, 8):
                        scales[day] = []

                    for timer in timers:
                        time = self.get_timer_time(timer)
                        action = self.get_timer_action(timer)

                        x_point = time.hour * 10 + time.minute / 6 + 1
                        days = timer['weekday'] if 'weekday' in timer else range(1, 8)
                        for day in days:
                            if action == 'on':
                                scales[day].append({action: x_point})
                            else:
                                scales[day][-1][action] = x_point

                    #draw.stroke_width = 2
                    #draw.stroke_antialias = False
                    draw.stroke_color = Color(color['workday'])
                    for day in range(1, 6):
                        self.draw_action(draw, day, scales)

                    draw.stroke_color = Color(color['holiday'])
                    for day in range(6, 8):
                        self.draw_action(draw, day, scales)

                    with Image(filename=os.path.join(self.path_images, 'scale.png')) as img:
                        #with Color('#FFFFFF') as white:
                        #    twenty_percent = int(65535 * 0.1)  # Note: percent must be calculated from Quantum
                        #    img.transparent_color(white, alpha=0.0, fuzz=twenty_percent)

                        draw.draw(img)
                        try:
                            img.save(filename=os.path.join(self.path_images, outlet+'_bar.png'))
                        except Exception as e:
                            exit('Cannot write an image: {}_bar.png'.format(outlet))


    def draw_action(self, draw, day, coords):
        y_point = (day - 1) * 3 + 2
        x_points = coords[day]
        for x_point in x_points:

            draw.line((x_point['on'], y_point),
                      (x_point['off'], y_point))

            draw.line((x_point['on'], y_point + 1),
                      (x_point['off'], y_point + 1))


relay_scales()

exit(0)
