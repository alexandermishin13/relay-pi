#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os, argparse
from relay import Relay

class relay_set(Relay):

    def __init__(self):
        super().__init__()

        parser = argparse.ArgumentParser(description="Switches the outlets into a new state or mode")

        parser.add_argument('relays',
            nargs = '*',
            metavar = 'outletID=state',
            help = 'Pairs of outlet IDs and target states delimited by an equals sign')
        parser.add_argument('--list-states',
            action = 'store_true',
            help = "Lists all possible target states")
    
        args = parser.parse_args()

        if args.list_states:
            self.list_states();
        elif args.relays:
            self.switch_relays(args.relays)
        else:
            parser.print_help()


    """ Do the job on the relays
    """
    def switch_relays(self, relays):
        for arg in relays:
            try:
                outlet_id, action = arg.split("=")
                outlet_id = outlet_id.strip()
                action = action.strip()
                self.switch_outlet(outlet_id, action)
            except ValueError:
                print(f'The argument "{arg}" is not recognized. Ignored');


    """ Lists all known states
    """
    def list_states(self):
        for action in self.known_states:
            print(action)

        mode = 1;
        while mode <= self.modes_number:
            print(f'mode{mode}')
            mode += 1;


if __name__ == "__main__":
    relay_set()
